class Api::V1::MonstersController < Api::V1::BaseController  
  before_action :set_monsters, only: [:index, :create]
  before_action :set_monster, only: [:show, :edit, :update, :destroy]

  def index
    dir = params[:dir] || "asc"
    @monsters = case params[:order]
    when nil
       @monsters
    when "weakness"
      @monsters.sort do |a, b|
        winner = winner(a.monster_type, b.monster_type)
        case
        when a.monster_type.to_sym == winner
          dir == "asc" ? 1 : -1
        when b.monster_type.to_sym == winner
          dir == "asc" ? -1 : 1
        end
      end
    else
      @monsters.order("#{params[:order]} #{dir}")
    end

    expose @monsters
  end

  def show
    expose @monster
  end
  
  def create
    expose @monsters.create(monster_params), status: :created
  end

  def update
    expose @monster.update_attributes(monster_params)
  end

  def destroy
    expose @monster.destroy
  end
  
  private

  def set_monsters
    @monsters = current_user.monsters
  end

  def set_monster
    @monster = Monster.find(params[:id])
  end

  def winner(p1, p2)
    Monster::RULES[p1.to_sym][p2.to_sym]
  end

  def monster_params
    params.require(:monster).permit(:name, :power, :monster_type)
  end
end