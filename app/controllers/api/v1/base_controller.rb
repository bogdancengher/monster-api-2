class Api::V1::BaseController < RocketPants::Base
  include Devise::Controllers::Helpers
  before_filter :authenticate_user!
  version 1
end