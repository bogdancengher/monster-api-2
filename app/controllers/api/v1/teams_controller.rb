class Api::V1::TeamsController < Api::V1::BaseController 
  before_action :set_teams, only: [:index, :create]
  before_action :set_team, only: [:show, :edit, :update, :destroy, :monsters, :add_monster]

  def index
    expose @teams
  end

  def show
    expose @team
  end
  
  def create
    expose @teams.create(team_params), status: :created
  end

  def update
    expose @team.update_attributes(team_params)
  end

  def destroy
    expose @team.destroy
  end

  def monsters
    expose @team.monsters
  end

  def add_monster
    monster = Monster.find(team_params[:monster_id])
    @team.monsters << monster
    expose @team.monsters
  end

  
  private

  def set_teams
    @teams = current_user.teams
  end

  def set_team
    @team = Team.find(params[:id])
  end

  def team_params
    params.require(:team).permit(:name, :monster_id)
  end
end