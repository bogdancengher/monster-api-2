class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :trackable, :validatable, :token_authenticatable
  devise :omniauthable, :omniauth_providers => [:facebook]

  has_many :monsters, dependent: :destroy
  has_many :teams, dependent: :destroy
  

  def self.find_or_create_by_auth(auth_data)
    user = self.find_or_create_by(uid: auth_data["uid"])
    if user.email != auth_data.extra.raw_info.email
      user.email = auth_data.extra.raw_info.email
      user.name = auth_data.extra.raw_info.name
      user.password = Devise.friendly_token[0,20]
      user.save
    end
    return user
  end
end
