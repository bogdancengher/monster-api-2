class Team < ActiveRecord::Base
  validates :name, presence: true
  belongs_to :user
  has_many :monsters_to_teams, dependent: :destroy
  has_many :monsters, through: :monsters_to_teams
  validate :group_count

  def group_count
    errors.add(:base, "You can only have a maximum of 3 teams") if user.teams.count == 3
  end
end
