class Monster < ActiveRecord::Base
  validates :name, presence: true
  validates :power, presence: true
  validates :monster_type, presence: true
  validates_inclusion_of :monster_type, in: %w(fire water earth electric wind)
  validates :user_id, presence: true
  validate :group_count

  belongs_to :user
  has_many :monsters_to_teams, dependent: :destroy
  has_many :teams, through: :monsters_to_teams

  def group_count
   errors.add(:base, "You can only have a maximum of 20 monsters") if user.monsters.count == 20
  end

  TYPES = %w(fire water earth electric wind)

  RULES = {
    :fire     => {:fire => :fire, :water => :fire, :earth => :fire, :electric => :fire, :wind => :fire},
    :water    => {:fire => :water, :water => :water, :earth => :earth, :electric => :electric, :wind => :wind},
    :earth => {:fire => :earth, :water => :earth, :earth => :earth, :electric => :electric, :wind => :wind},
    :electric => {:fire => :electric, :water => :electric, :earth => :electric, :electric => :electric, :wind => :wind},
    :wind => {:fire => :wind, :water => :wind, :earth => :wind, :electric => :wind, :wind => :wind}
  }

end
