class CreateMonsters < ActiveRecord::Migration
  def change
    create_table :monsters do |t|
      t.integer :user_id
      t.string :name
      t.string :power
      t.string :monster_type

      t.timestamps null: false
    end
  end
end
