describe "Monsters API Monster" do
  before(:all) do
    @user = FactoryGirl.create(:user)
  end

  it 'gets list of monsters' do
    FactoryGirl.create(:monster, user_id: @user.id)
    get "/api/v1/monsters?auth_token=#{@user.authentication_token}"
    expect(response).to be_success
    expect(json['response'].length).to eq(1)
  end

  it 'creates monster' do
    post "/api/v1/monsters", {monster: {name: "Test Monster", power: "test power", monster_type: "fire"}, auth_token: @user.authentication_token}
    expect(response.status).to eq(201)
  end

  it 'destroyes a monster' do
    monster = FactoryGirl.create(:monster, user_id: @user.id)
    delete "/api/v1/monsters/#{monster.id}?auth_token=#{@user.authentication_token}"
    expect(response).to be_success
    expect(@user.monsters.count).to eq(0)
  end

  it 'validates for the maximum number of monsters to be 20' do
    1.upto(21) do |index|
      post "/api/v1/monsters", {monster: {name: "Test Monster", power: "test power", monster_type: "fire"}, auth_token: @user.authentication_token}
      if index == 21
        expect(response.status).to eq(422)
      else
        expect(response.status).to eq(201)
      end
    end
  end

end