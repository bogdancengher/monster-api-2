describe "Monsters API Team" do
  before(:all) do
    @user = FactoryGirl.create(:user)
  end

  it 'gets list of teams' do
    FactoryGirl.create(:team, user_id: @user.id)
    get "/api/v1/teams?auth_token=#{@user.authentication_token}"
    expect(response).to be_success
    expect(json['response'].length).to eq(1)
  end

  it 'create team' do
    post "/api/v1/teams", {team: {name: "Test Team"}, auth_token: @user.authentication_token}
    expect(response.status).to eq(201)
  end

  it 'destroyes a team' do
    team = FactoryGirl.create(:team, user_id: @user.id)
    delete "/api/v1/teams/#{team.id}?auth_token=#{@user.authentication_token}"
    expect(response).to be_success
    expect(@user.teams.count).to eq(0)
  end

  it 'validates for the maximum number of teams to be 3' do
    1.upto(4) do |index|
      post "/api/v1/teams", {team: {name: "Test Team"}, auth_token: @user.authentication_token}
      if index == 4
        expect(response.status).to eq(422)
      else
        expect(response.status).to eq(201)
      end
    end
  end

  it 'adds a monster to a team' do
    monster = FactoryGirl.create(:monster, user_id: @user.id)
    team = FactoryGirl.create(:team, user_id: @user.id)
    post "/api/v1/teams/#{team.id}/add_monster", {team: {monster_id: monster.id}, auth_token: @user.authentication_token}
    get "/api/v1/teams/#{team.id}/monsters?auth_token=#{@user.authentication_token}"
    expect(response).to be_success
    expect(json['response'].length).to eq(1)
  end

end