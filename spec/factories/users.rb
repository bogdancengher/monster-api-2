require 'faker'
FactoryGirl.define do
  factory :user do |f|
    f.email { Faker::Internet.email}
    f.name { Faker::Name.name }
    f.password              "passwords"
    f.password_confirmation "passwords"
    f.authentication_token  { Faker::Lorem.characters(10) }
  end
end