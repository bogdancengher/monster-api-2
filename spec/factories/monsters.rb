require 'faker'
FactoryGirl.define do
  factory :monster do |f|
    f.name         { Faker::Name.name }
    f.power        { Faker::Name.name }
    f.monster_type ["fire", "water", "earth", "electric", "wind"].sample
  end
end