require 'faker'
FactoryGirl.define do
  factory :team do |f|
    f.name         { Faker::Name.name }
  end
end